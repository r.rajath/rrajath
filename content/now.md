---
title: "What am I doing right now?"
date: 2020-06-15T18:23:51-07:00
type: now
draft: false
---

### Reading
* [Digital Minimalism](https://www.amazon.com/Digital-Minimalism-Choosing-Focused-Noisy-ebook/dp/B07DBRBP7G)
* [What to write down when you're reading to learn](https://acesounderglass.com/2020/06/10/what-to-write-down-when-youre-reading-to-learn/)
* [Building a second brain](https://maggieappleton.com/basb/)

### Health
* Working out at home
* Meditating

### Projects
* Moved my blog from Jekyll to Hugo and now customizing some templates
* IndieWeb integration
* Self-hosting my data using Nextcloud on my Raspberry Pi

### What else?
* Playing chess, a lot
* Trying to get back into blogging regularly again
